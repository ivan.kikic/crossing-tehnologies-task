import './App.css';
import { useFetch } from './hooks/useFetch'
import ImageList from './components/Image';
import { useState } from 'react';

function App() {
  const url = 'https://api-dev.insidetrak.interactive1.com/homepage/get-latest-images'
  const {data: images, isPending, handleClick, handleSeen} = useFetch(url)
  const [isFixed, setIsFixed] = useState(true)
  
  const handleToggle = () => {
    setIsFixed(!isFixed)
  }

  const toggleIsActive = (id) => {
      var btns = document.getElementsByClassName("btn");
      var dropBtns = document.getElementsByClassName("btn-drop")

      for (var i = 0; i < btns.length; i++) {
          if(i === id) {
            btns[i].className += " active";
            dropBtns[i].className += " active";
          } else {
            if (btns[i].classList.contains("active")) {
              btns[i].classList.toggle("active");
            }
            if (dropBtns[i].classList.contains("active")) {
              dropBtns[i].classList.toggle("active");
            }
          }
      }
  }

  const  hamburger = () => {
    var x = document.getElementById("myDIV");
    var y = document.getElementById("dropdown")
    y.classList.toggle("active");
    if (x.style.visibility === "visible") {
      x.style.visibility = "hidden";
    } else {
      x.style.visibility = "visible";
    }
  }




  const [typeOfImages, setTypeOfImages] = useState("all")
  const handleSeenImages = (type) => {
      setTypeOfImages(type)
  }


  return (
    <div className="App">
      <div className="settings">
        <button className="toggleButton" onClick={handleToggle}>{isFixed ? "Set image heights auto" : "Set image heights fixed"}</button>
        <div className="dropdown" onClick={hamburger}>
          <button id="dropdown" className="dropbtn">
            <img src="/ham.png" alt="dropdown-icon" />
          </button>
        </div>
        <div className="settings-seen-images">
          <button className="btn active" onClick={() => { handleSeenImages("all"); toggleIsActive(0) }}>All images</button>
          <button className="btn" onClick={() => { handleSeenImages("seen"); toggleIsActive(1) }}>Seen images</button>
          <button className="btn" onClick={() => { handleSeenImages("unseen"); toggleIsActive(2) }}>Unseen images</button>
        </div>
      </div>
      <div id="myDIV" className="dropdown-content">
            <button className="btn-drop active" onClick={() => { handleSeenImages("all"); toggleIsActive(0) }}>All images</button>
            <button className="btn-drop" onClick={() => { handleSeenImages("seen"); toggleIsActive(1) }}>Seen images</button>
            <button className="btn-drop" onClick={() => { handleSeenImages("unseen"); toggleIsActive(2) }}>Unseen images</button>
        </div>
      <div className='image-list'>
        <h2>Image list</h2>
        <ul>
          {isPending && <div>Loading images..</div>}
          {images && images.map(image => (
            <ImageList
              key={image.id}
              image={image}
              handleClick={handleClick}
              handleSeen={handleSeen}
              isFixed={isFixed}
              typeOfImages={typeOfImages}
            />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
