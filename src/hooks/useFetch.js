import { useEffect, useState } from 'react'

export const useFetch = (url) => {
    const [data, setData] = useState(null)
    const [isPending, setIsPending] = useState(false)

    const handleClick = (id) => {
        setData((prevData) => {
            return prevData.filter((data) => {
            return data.id !== id
            })
        })
    }

    useEffect(() => {
        const fetchData = async () => {
            setIsPending(true)
            const res = await fetch(url)
            const json = await res.json()
            setIsPending(false)
            setData(json.data)
        }

    fetchData()
    }, [url])

    return ({data, isPending, handleClick})
}
