import { useState } from 'react'
import './Image.css'

export default function ImageList(props) {
    const {id, title, authorScreenName, publishedOn, description, thumbnailUrls} = props.image
    const handleClick = props.handleClick
    const typeOfImages = props.typeOfImages
    const isFixed = props.isFixed
    const [seenUrl, setSeenUrl] = useState("/unseen.png")
    const imedia_1024 = thumbnailUrls.imedia_1024

    const handleSeen = () => {
        if (seenUrl === "/unseen.png") {
            setSeenUrl("/seen.jpg")
        } else {
            setSeenUrl("/unseen.png")
        }
    }

    if (typeOfImages === "seen") {
        if(seenUrl === "/seen.jpg") {
            return (
                <li key={id}>
                    <div className='options'>
                        <p>Actions</p>
                        <div className='options-images'>
                            <img title="Toggle seen/unseen" onClick={() => {handleSeen()}} className="seen-img" src={seenUrl} alt="status" />
                            <img title="Delete this post"onClick={() => {handleClick(id)}} className="trash-img" src="/trash.png" alt="status" />
                        </div>
                    </div>
                    <img src={imedia_1024} alt="imageAlt" className={isFixed ? "fixed" : null}/>
                    <h3>{title}</h3>
                    <p><strong>Author screen name: </strong>{authorScreenName}</p>
                    <p><strong>Published: </strong>{publishedOn}</p>
                    <p><strong>Description: </strong>{description}</p>
                </li>
            )
        } 
    } else if(typeOfImages === "unseen") {
        if(seenUrl === "/unseen.png") {
            return (
                <li key={id}>
                    <div className='options'>
                        <p>Actions</p>
                        <div className='options-images'>
                            <img title="Toggle seen/unseen" onClick={() => {handleSeen()}} className="seen-img" src={seenUrl} alt="status" />
                            <img title="Delete this post"onClick={() => {handleClick(id)}} className="trash-img" src="/trash.png" alt="status" />
                        </div>
                    </div>
                    <img src={imedia_1024} alt="imageAlt" className={isFixed ? "fixed" : null}/>
                    <h3>{title}</h3>
                    <p><strong>Author screen name: </strong>{authorScreenName}</p>
                    <p><strong>Published: </strong>{publishedOn}</p>
                    <p><strong>Description: </strong>{description}</p>
                </li>
            )
        }
    } else {
        return (
            <li key={id}>
                <div className='options'>
                    <p>Actions</p>
                    <div className='options-images'>
                        <img title="Toggle seen/unseen" onClick={() => {handleSeen()}} className="seen-img" src={seenUrl} alt="status" />
                        <img title="Delete this post"onClick={() => {handleClick(id)}} className="trash-img" src="/trash.png" alt="status" />
                    </div>
                </div>
                <img src={imedia_1024} alt="imageAlt" className={isFixed ? "fixed" : null}/>
                <h3>{title}</h3>
                <p><strong>Author screen name: </strong>{authorScreenName}</p>
                <p><strong>Published: </strong>{publishedOn}</p>
                <p><strong>Description: </strong>{description}</p>
            </li>
        )
    }
}
